#Python3
import csv
import urllib3
import urllib.request
import json
import time
from twython import Twython, TwythonError


DEBUG = False
if not DEBUG:
  twitter = Twython(APP_KEY, APP_SECRET,OAUTH_TOKEN, OAUTH_TOKEN_SECRET)


http = urllib3.PoolManager()
url = 'https://firms.modaps.eosdis.nasa.gov/active_fire/c6/text/MODIS_C6_Central_America_24h.csv'
r = http.request('GET', url, preload_content=False)

bomberosTwitter = {"Costa Rica":'@BomberosCR', "Nicaragua":'@BomberosNicas', "Honduras":'@BomberosHn',"Guatemala":'@BVoluntariosGT', "El Salvador":'@BomberosES', "Panama":'@BCBRP'}

data = r.read(100000)

cr = csv.reader(data)

with open('MODIS_C6_Central_America_24h.csv', 'wb') as f:
    f.write(data)


r = csv.reader(open('MODIS_C6_Central_America_24h.csv'), delimiter=',')

for row in r:
	lat = row[0]
	long = row[1]

	fe = time.strftime("%Y-%m-%d")

	if str(fe) == row[5]:
		googleubic = ("http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=true") % (lat,long)

		response = urllib.request.urlopen(googleubic).read().decode('utf8')

		data = json.loads(str(response))
		tam = len(data['results'])

		if data['status'] == "OK":

			pais = data['results'][tam-1]['formatted_address']
			direccion = data['results'][0]['formatted_address']

			if  pais == "Costa Rica" or  pais == "Nicaragua" or pais == "Honduras" or pais == "Guatemala" or pais == "El Salvador" or pais == "Panama":

				

				if DEBUG:
					print(pais)
					print(row)
					print(fe)

					mapa = "http://maps.google.com/maps/api/staticmap?center=%s,%s&zoom=14&size=800x900&markers=%s,%s&path=color:0xff0000ff|weight:5&sensor=false&maptype=hybrid&format=jpg" % (lat,long,lat,long)
					urllib.request.urlretrieve(mapa, "image_fire.jpg")

					mensaje = "ALERTA de incendio %s en:%s(lat:%s,long:%s) según FIRMS NASA" % (bomberosTwitter[pais], direccion, lat, long)
					print(mensaje)

				if direccion.startswith("Unnamed Road,"):
					direccion = direccion.replace("Unnamed Road, ","")


				

				if not DEBUG:
					print(bomberosTwitter[pais])
					print(direccion)
					print(row)

					if not DEBUG:
						mensaje = "ALERTA de incendio %s en:%s(lat:%s,long:%s) según satelite MODIS NRT C6" % (bomberosTwitter[pais], direccion, lat, long)

						mapa = "http://maps.google.com/maps/api/staticmap?center=%s,%s&zoom=14&size=800x900&markers=%s,%s&path=color:0xff0000ff|weight:5&sensor=false&maptype=roadmap&format=jpg" % (lat,long,lat,long)
						urllib.request.urlretrieve(mapa, "image_fire.jpg")

						photo = open('image_fire.jpg', 'rb')
						response = twitter.upload_media(media=photo)

						twitter.update_status(status=mensaje, media_ids=[response['media_id']])
	else:
		print("No hay incendios al momento")