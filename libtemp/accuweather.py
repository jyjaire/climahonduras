from twython import Twython, TwythonError
from bs4 import BeautifulSoup
import requests

DEBUG = True
if not DEBUG:
  twitter = Twython(APP_KEY, APP_SECRET,OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

class Temperatura:

   def __init__(self, url, ciudad):
      self.url = url
      self.ciudad = ciudad
   
   def getTemp(url, ciudad):

     data = requests.get(url).content
     soup = BeautifulSoup(data,"html5lib")
     temp = soup.find_all('span', class_="temp")
     if not temp:
      temp = soup.find_all('strong', class_="temp")
     feel = soup.find_all('span', class_="realfeel")

     rfeel = feel[0].text
     rfeel1 = rfeel[10:]
     #print(temp)
     max = temp[0].text
     min = temp[1].text

     text = "La temp. max. en "+ciudad+" es de "+ max + "C y la min. de "+min+"C, la sensación es de "+ rfeel1+"C "
     
     if not DEBUG:
        try:
           twitter.update_status(status=text)
        except TwythonError as e:
           print(e)

     else:
         print(text)


   def getTempTomorrow(url, ciudad):

     data = requests.get(url).content
     soup = BeautifulSoup(data,"html5lib")
     temp = soup.find_all('span', class_="temp")
     feel = soup.find_all('span', class_="realfeel")

     rfeel = feel[1].text
     rfeel1 = rfeel[16:]#Posibilidad de lluvia

     sens = feel[0].text
     sens_temp = sens[10:] #sensacion termica

     max = temp[0].text
     min = temp[1].text

     text = "La temp. max. en "+ciudad+" será de "+ max + "C y la min. de "+min+"C, la sensación será de "+ sens_temp+"C, Posibilidad de lluvia:"+rfeel1

     if not DEBUG:
        try:
           twitter.update_status(status=text)
        except TwythonError as e:
           print(e)

     else:
         print(text)
   
   #En el caso de Honduras el parametro URL sera http://www.accuweather.com/es/browse-locations/cac/hn
   def getDepartamentos(url):

     data = requests.get(url).content
     soup = BeautifulSoup(data,"lxml")
     info = soup.find_all('div', class_="info")

     filename = "departamentos.txt"

     target = open(filename, 'w')
     info.pop()

     for i in info:
      depto = i.text

      link = i.find('a')

      linea = "%s|%s" % (str(depto.replace('\n','')), str(link['href']))
      target.write(linea+'\n')

     target.close()
     if not DEBUG:
        try:
           twitter.update_status(status=text)
        except TwythonError as e:
           print(e)

     else:
         print(info)


   def getMunicipios():



     filename = "municipios.txt"
     deptos = "departamentos.txt"

     lineas = [linea.rstrip('\n') for linea in open(deptos)]
     #print(lineas[0].split('|'))
     target = open(filename, 'w')

     for datos in lineas:
      
      t = datos.split('|')
      data = requests.get(t[1]).content
      soup = BeautifulSoup(data,"lxml")
      info = soup.find_all('div', class_="info")

     
      info.pop()

      for i in info:
       depto = i.text

       link = i.find('a')

       linea = "%s|%s|%s" % (str(t[0]),str(depto.replace('\n','')), str(link['href']))
       print(linea)
       
     
       target.write(linea+'\n')

     target.close()

     if not DEBUG:
        try:
           twitter.update_status(status=text)
        except TwythonError as e:
           print(e)

     else:
         print(info)
     
    